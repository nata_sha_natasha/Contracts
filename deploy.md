# Развертывание смарт-контракта

## Настройки

### **node и npm**

Для начала необходимо обновить список пакетов в системе:
```
sudo apt-get update
```
Команда update используется для синхронизации и обновления индексных файлов пакетов.

Далее необходимо установить следующие пакеты:
```
sudo apt-get install git-core curl build-essential openssl libssl-dev auto-apt checkinstall
```
* git-core - система контроля версий

* curl - утилита командной строки, которая содержит в себе набор библиотек, в которых реализуются базовые возможности работы с URL страницами и передачи файлов

* build-essential - информационный список пакетов необходимых для сборки

* openssl - программа реализующая протокол безопасных соединений (SSL) и инструменты криптографии

* libssl-dev - библиотеки разработки SSL, файлы заголовков и документация

* auto-apt - auto-apt следит за попытками программ получить доступ к файлам, выполняющихся в рамках её окружения, и если программа пытается получить доступ к файлу, который принадлежит неустановленному пакету, то auto-apt устанавливает этот пакет с помощью apt-get

* checkinstall - утилита, которая создаtn инсталяционные пакеты, RPM или Slackware из исходных кодов, распространяемых в tgz-архивах

Скачать и установить менеджер версий NVM с помощью следующей команды:
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash 
```
```v0.33.11``` - последняя версия на данный момент

После завершения установки необходимо перезапустить терминал.

Далее устанавливается nodejs с помощью следующей команды:
```
nvm install версия
```
Чтобы использовать определенную версию из установленных, выполняется команда:
```
nvm use версия
```
Просмотреть версию node и npm можно следующим образом:
```
node -v
npm -v
```
Если необходимо обновить npm до последней версии, выполняется следующее:
```
npm i npm -g
```

### **solc**

Для установки компилятора необходимо выполнить следующую команду:
```
npm i solc -g
```

### **geth**

Для начала устанваливается утилита для управления репозиториями:
```
sudo apt-get install software-properties-common
```
После чего добавляем необходимый репозиторий:
```
sudo add-apt-repository -y ppa:ethereum/ethereum
```
Обновляем список пакетов в системе:
```
sudo apt-get update
```
Устанавливается стабильная версия geth с помощью следующей команды:
```
sudo apt-get install ethereum
```

## Компиляция контракта

Контракт сохраняется в файле с расширением ```.sol``` (Например, ```token.sol```). Далее необходимо перейти в каталог, который содержит данный контракт.

Для компиляции необходимо выполнить следующие команды:
```
solcjs --abi token.sol 
solcjs --bin token.sol
```

Для данного контракта после выполнения созданы следующие файлы:

* token_sol_NewToken.abi
* token_sol_NewToken.bin
* token_sol_owned.abi
* token_sol_owned.bin
* token_sol_Token.abi
* token_sol_Token.bin
* token_sol_tokenRecipient.abi
* token_sol_tokenRecipient.bin

## Запуск geth-узла

Первый узел запущен для майнинга. Второй узел необходимо запустить следующим образом:
```
geth attach node2/geth.ipc
```

## Развертывание контракта

Для начала необходимо разблокировать аккаунт:
```
> personal.unlockAccount(eth.coinbase)
```
В дальнейшем будет использовать контракт с названием NewToken.

Следующая команда записывает в переменную ```abi``` содержимое файла ```token_sol_NewToken.abi```:
```
> var abi = [{"constant":false,"inputs":[{"name":"newSellPrice","type":"uint256"},{"name":"newBuyPrice","type":"uint256"}],"name":"setPrices","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"sellPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"currentChallenge","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"mintedAmount","type":"uint256"}],"name":"mintToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_value","type":"uint256"}],"name":"burnFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"buyPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"buy","outputs":[{"name":"amount","type":"uint256"}],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"frozenAccount","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"},{"name":"_extraData","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"sell","outputs":[{"name":"revenue","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"answerToCurrentReward","type":"uint256"},{"name":"nextChallenge","type":"uint256"}],"name":"rewardMathGeniuses","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"target","type":"address"},{"name":"freeze","type":"bool"}],"name":"freezeAccount","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"initialSupply","type":"uint256"},{"name":"tokenName","type":"string"},{"name":"tokenSymbol","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"target","type":"address"},{"indexed":false,"name":"frozen","type":"bool"}],"name":"FrozenFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Burn","type":"event"}];
```
В переменную ```bytecode``` записывается строка вида '```0x```<содержимое файла ```token_sol_NewToken.bin```>'
```
> var bytecode = '0x60806040526012600360006101000a81548160ff021916908360ff16021790555060016009553480156200003257600080fd5b5060405162001d9838038062001d98833981018060405281019080805190602001909291908051820192919060200180518201929190505050828282336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600360009054906101000a900460ff1660ff16600a0a8302600481905550600454600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000208190555081600190805190602001906200012a92919062000150565b5080600290805190602001906200014392919062000150565b50505050505050620001ff565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106200019357805160ff1916838001178555620001c4565b82800160010185558215620001c4579182015b82811115620001c3578251825591602001919060010190620001a6565b5b509050620001d39190620001d7565b5090565b620001fc91905b80821115620001f8576000816000905550600101620001de565b5090565b90565b611b89806200020f6000396000f30060806040526004361061013e576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806305fefda71461014357806306fdde031461017a578063095ea7b31461020a57806318160ddd1461026f57806323b872dd1461029a578063313ce5671461031f57806342966c68146103505780634b7503341461039557806351bdd585146103c057806370a08231146103eb57806379c650681461044257806379cc67901461048f5780638620410b146104f45780638da5cb5b1461051f57806395d89b4114610576578063a6f2ae3a14610606578063a9059cbb14610624578063b414d4b614610671578063cae9ca51146106cc578063dd62ed3e14610777578063e4849b32146107ee578063e5f952d71461082f578063e724529c14610866578063f2fde38b146108b5575b600080fd5b34801561014f57600080fd5b5061017860048036038101908080359060200190929190803590602001909291905050506108f8565b005b34801561018657600080fd5b5061018f610965565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156101cf5780820151818401526020810190506101b4565b50505050905090810190601f1680156101fc5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34801561021657600080fd5b50610255600480360381019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190505050610a03565b604051808215151515815260200191505060405180910390f35b34801561027b57600080fd5b50610284610a90565b6040518082815260200191505060405180910390f35b3480156102a657600080fd5b50610305600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190505050610a96565b604051808215151515815260200191505060405180910390f35b34801561032b57600080fd5b50610334610bc3565b604051808260ff1660ff16815260200191505060405180910390f35b34801561035c57600080fd5b5061037b60048036038101908080359060200190929190505050610bd6565b604051808215151515815260200191505060405180910390f35b3480156103a157600080fd5b506103aa610cda565b6040518082815260200191505060405180910390f35b3480156103cc57600080fd5b506103d5610ce0565b6040518082815260200191505060405180910390f35b3480156103f757600080fd5b5061042c600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610ce6565b6040518082815260200191505060405180910390f35b34801561044e57600080fd5b5061048d600480360381019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190505050610cfe565b005b34801561049b57600080fd5b506104da600480360381019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190505050610eb1565b604051808215151515815260200191505060405180910390f35b34801561050057600080fd5b506105096110cb565b6040518082815260200191505060405180910390f35b34801561052b57600080fd5b506105346110d1565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561058257600080fd5b5061058b6110f6565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156105cb5780820151818401526020810190506105b0565b50505050905090810190601f1680156105f85780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b61060e611194565b6040518082815260200191505060405180910390f35b34801561063057600080fd5b5061066f600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803590602001909291905050506112f9565b005b34801561067d57600080fd5b506106b2600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050611308565b604051808215151515815260200191505060405180910390f35b3480156106d857600080fd5b5061075d600480360381019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919080359060200190929190803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509192919290505050611328565b604051808215151515815260200191505060405180910390f35b34801561078357600080fd5b506107d8600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506114ab565b6040518082815260200191505060405180910390f35b3480156107fa57600080fd5b50610819600480360381019080803590602001909291905050506114d0565b6040518082815260200191505060405180910390f35b34801561083b57600080fd5b506108646004803603810190808035906020019092919080359060200190929190505050611675565b005b34801561087257600080fd5b506108b3600480360381019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291908035151590602001909291905050506116e1565b005b3480156108c157600080fd5b506108f6600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050611806565b005b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561095357600080fd5b81600781905550806008819055505050565b60018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156109fb5780601f106109d0576101008083540402835291602001916109fb565b820191906000526020600020905b8154815290600101906020018083116109de57829003601f168201915b505050505081565b600081600660003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055506001905092915050565b60045481565b6000600660008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020548211151515610b2357600080fd5b81600660008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540392505081905550610bb88484846118a4565b600190509392505050565b600360009054906101000a900460ff1681565b600081600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205410151515610c2657600080fd5b81600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540392505081905550816004600082825403925050819055503373ffffffffffffffffffffffffffffffffffffffff167fcc16f5dbb4873280815c1ee09dbd06736cffcc184412cf7a71a0fdb75d397ca5836040518082815260200191505060405180910390a260019050919050565b60075481565b60095481565b60056020528060005260406000206000915090505481565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610d5957600080fd5b80600560008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540192505081905550806004600082825401925050819055506000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1660007fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef836040518082815260200191505060405180910390a38173ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef836040518082815260200191505060405180910390a35050565b600081600560008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205410151515610f0157600080fd5b600660008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020548211151515610f8c57600080fd5b81600560008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254039250508190555081600660008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540392505081905550816004600082825403925050819055508273ffffffffffffffffffffffffffffffffffffffff167fcc16f5dbb4873280815c1ee09dbd06736cffcc184412cf7a71a0fdb75d397ca5836040518082815260200191505060405180910390a26001905092915050565b60085481565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b60028054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561118c5780601f106111615761010080835404028352916020019161118c565b820191906000526020600020905b81548152906001019060200180831161116f57829003601f168201915b505050505081565b6000600854348115156111a357fe5b04905080600560003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054101515156111f457600080fd5b80600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254019250508190555080600560003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600082825403925050819055503373ffffffffffffffffffffffffffffffffffffffff163073ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef836040518082815260200191505060405180910390a380905090565b6113043383836118a4565b5050565b600a6020528060005260406000206000915054906101000a900460ff1681565b6000808490506113388585610a03565b156114a2578073ffffffffffffffffffffffffffffffffffffffff16638f4ffcb1338630876040518563ffffffff167c0100000000000000000000000000000000000000000000000000000000028152600401808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018481526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200180602001828103825283818151815260200191508051906020019080838360005b83811015611432578082015181840152602081019050611417565b50505050905090810190601f16801561145f5780820380516001836020036101000a031916815260200191505b5095505050505050600060405180830381600087803b15801561148157600080fd5b505af1158015611495573d6000803e3d6000fd5b50505050600191506114a3565b5b509392505050565b6006602052816000526040600020602052806000526040600020600091509150505481565b600081600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020541015151561152057600080fd5b81600560003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254019250508190555081600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540392505081905550600754820290503373ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051600060405180830381858888f19350505050158015611607573d6000803e3d6000fd5b503073ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef846040518082815260200191505060405180910390a3809050919050565b6009546003830a14151561168857600080fd5b6001600560003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540192505081905550806009819055505050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561173c57600080fd5b80600a60008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055507f48335238b4855f35377ed80f164e8c6f3c366e54ac00b96a6402d4a9814a03a58282604051808373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001821515151581526020019250505060405180910390a15050565b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561186157600080fd5b806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b60008273ffffffffffffffffffffffffffffffffffffffff16141515156118ca57600080fd5b80600560008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020541015151561191857600080fd5b600560008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205481600560008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205401101515156119a757600080fd5b600a60008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151515611a0057600080fd5b600a60008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151515611a5957600080fd5b80600560008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254039250508190555080600560008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600082825401925050819055508173ffffffffffffffffffffffffffffffffffffffff168373ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef836040518082815260200191505060405180910390a35050505600a165627a7a72305820f791cfc1fce76a8792d1cb6d6f5d1f6534b79fc156ebf03f75774f16f2c4d6200029';
```
Далее выполняется следующая настройка:
```
> var tokenContract = eth.contract(abi);
```
После чего разворачивается контракт:
```
> var deploy = {from:eth.coinbase, data:bytecode, gas: 2000000};
> var TokenPartialInstance = tokenContract.new(10000, "Name", "NM", deploy);
```
В функцию new передаются все параметры, которые ожидает конструктор контракта в виде списка, разделенном запятыми. Конечным параметром всегда должна быть переменная deploy, указанная в предыдущей строке. 

Далее необходимо подождать минуту, чтобы контракт выполнился. Для проверки выполните следующую команду:
```
> TokenPartialInstance
```
Если поле ```address``` заполнено, то контракт готов к работе.

С помощью следующей команды в переменную записывается ссылка на развернутый контракт:
```
> var TokenInstance = tokenContract.at(TokenPartialInstance.address)
```
Данную переменную можно использовать для выполнения функций контракта, например, для установки цены покупки и продажи:
```
> TokenInstance.setPrices(newSellPrice, newBuyPrice)
```