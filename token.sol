pragma solidity ^0.4.18;

contract owned {
    address public owner;

    function owned() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function transferOwnership(address newOwner) onlyOwner public {
        owner = newOwner;
    }
}

interface tokenRecipient { 
    function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData) public; 
}

contract Token {
    string public name;
    string public symbol;
    uint8 public decimals = 18;
    uint256 public totalSupply;

    // Создает массивы для работы с балансом
    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;

    // Генерирует публичное событие для уведомления клиентов
    event Transfer(address indexed from, address indexed to, uint256 value);

    // Генерирует публичное событие для уведомления клиентов о количестве сожженых токенов
    event Burn(address indexed from, uint256 value);

    /**
     * Функция конструктора
     *
     * Инициализирует контракт с начальными количеством токенов для создателя контракта
     */
    function Token(
        uint256 initialSupply,
        string tokenName,
        string tokenSymbol
    ) public {
        totalSupply = initialSupply * 10 ** uint256(decimals);  // Обновить общий баланс с десятичной суммой
        balanceOf[msg.sender] = totalSupply;                    // Начисление баланса создателю контракта
        name = tokenName;                                       // Установка имени
        symbol = tokenSymbol;                                   // Установка символа
    }

    /**
     * Внутренний перевод, только по этому контракту
     */
    function _transfer(address _from, address _to, uint _value) internal {
        // Адрес не должен быть 0х0
        require(_to != 0x0);
        // Проверка, достаточно ли у отправителя токенов
        require(balanceOf[_from] >= _value);
        // Провекра переполнения
        require(balanceOf[_to] + _value > balanceOf[_to]);
        // Переменная для проверки проведения перевода
        uint previousBalances = balanceOf[_from] + balanceOf[_to];
        // Вычесть у отправителя
        balanceOf[_from] -= _value;
        // Зачислить получателю
        balanceOf[_to] += _value;
        Transfer(_from, _to, _value);
        // Используется для поиска ошибок в коде. Никогда не должен терпеть неудочу
        assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
    }

    /**
     * Перевод токенов
     *
     * Отправляет `_value` токенов к `_to` из учетной записи создателя
     *
     * @param _to Адрес получателя
     * @param _value Сумма для отправки
     */
    function transfer(address _to, uint256 _value) public {
        _transfer(msg.sender, _to, _value);
    }

    /**
     * Перевод токенов с другого адреса
     *
     * Отправляет `_value` токенов к `_to` из `_from`
     *
     * @param _from Адрес отправителя
     * @param _to Адрес получателя
     * @param _value Сумма для отправки
     */
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        require(_value <= allowance[_from][msg.sender]);     // Проверка допуска
        allowance[_from][msg.sender] -= _value;
        _transfer(_from, _to, _value);
        return true;
    }

    /**
     * Установить одобренный баланс для другого адреса
     *
     * Позволяет `_spender` тратить не больше, чем `_value` токенов от иммени создателя
     *
     * @param _spender Адрес, разрешенный для проведения перевода 
     * @param _value Максимальная сумма, которую можно потратить
     */
    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        return true;
    }

    /**
     * Установить одобренный баланс для другого адреса и уведомить
     *
     * Позволяет `_spender` тратить не больше, чем `_value` токенов от иммени создателя, а затем уведомить об этом
     *
     * @param _spender Адрес, разрешенный для проведения перевода 
     * @param _value Максимальная сумма, которую можно потратить
     * @param _extraData Дополнительная информация для отправки в утвержденный контракт
     */
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) public returns (bool success) {
        tokenRecipient spender = tokenRecipient(_spender);
        if (approve(_spender, _value)) {
            spender.receiveApproval(msg.sender, _value, this, _extraData);
            return true;
        }
    }

    /**
     * Сжечь (уничтожить) токены
     *
     * Удаляет `_value` токенов из ситемы необратимо
     *
     * @param _value Сумма для сжигания
     */
    function burn(uint256 _value) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value);   // Проверка, достаточно ли у отправителя токенов
        balanceOf[msg.sender] -= _value;            // Вычесть у отправителя
        totalSupply -= _value;                      // Обновить значение общего количества токенов
        Burn(msg.sender, _value);
        return true;
    }

    /**
     * Сжечь (уничтожить) токены с другого аккаунта
     *
     * Удаляет `_value` токенов из ситемы необратимо от имени `_from`.
     *
     * @param _from Адрес отправителя
     * @param _value Сумма для сжигания
     */
    function burnFrom(address _from, uint256 _value) public returns (bool success) {
        require(balanceOf[_from] >= _value);                // Проверка, достаточно ли баланса
        require(_value <= allowance[_from][msg.sender]);    // Проверка допуска
        balanceOf[_from] -= _value;                         // Вычесть из целевого баланса
        allowance[_from][msg.sender] -= _value;             // Вычесть из суммы отправителя
        totalSupply -= _value;                              // Обновить значение общего количества токенов
        Burn(_from, _value);
        return true;
    }
}

contract NewToken is owned, Token {

    uint256 public sellPrice;
    uint256 public buyPrice;
    uint public currentChallenge = 1;

    /* Создание массива для блокировки и разблокировки пользователя */
    mapping (address => bool) public frozenAccount;

    event FrozenFunds(address target, bool frozen);

    /* Инициализировать контракт с певоначальными данными */
    function NewToken(
        uint256 initialSupply,
        string tokenName,
        string tokenSymbol
    ) Token (initialSupply, tokenName, tokenSymbol) public {}

    /* Внутренний перевод, может быть вызван только по этому контракту */
    function _transfer(address _from, address _to, uint _value) internal {
        require (_to != 0x0);                                // Адрес не должен быть 0х0
        require (balanceOf[_from] >= _value);                // Проверка, достаточный ли у отправителя баланс
        require (balanceOf[_to] + _value >= balanceOf[_to]); // Проверка переполнения
        require(!frozenAccount[_from]);                      // Проверка, заблокирован ли отправитель.
        require(!frozenAccount[_to]);                        // Проверка, заблокирован ли получатель
        balanceOf[_from] -= _value;                          // Вычесть у отправителя
        balanceOf[_to] += _value;                            // Добавить к получателю
        Transfer(_from, _to, _value);
    }

    
    /// Отправляет `_value` токенов к `_to` из учетной записи создателя
    /// @param _to Адрес получателя
    /// @param _value Сумма для отправки
    function transfer(address _to, uint _value) public {
        _transfer(msg.sender, _to, _value);
    }

    /// @notice Создает `mintedAmount` токенов и отправляет их к `target`
    /// @param target Адрес получателя токенов
    /// @param mintedAmount Сумма для получения
    function mintToken(address target, uint256 mintedAmount) onlyOwner public {
        balanceOf[target] += mintedAmount;
        totalSupply += mintedAmount;
        Transfer(0, owner, mintedAmount);
        Transfer(owner, target, mintedAmount);
    }

    /* Блокировка и разблокировка пользователя */
    /// @notice `Заблокировать? Предотвратить | Разрешить` `target` отправлять и получать токены
    /// @param target Адрес для блокировки
    /// @param freeze Заблокировать или нет
    function freezeAccount(address target, bool freeze) onlyOwner public {
        frozenAccount[target] = freeze;
        FrozenFunds(target, freeze);
    }

    /// @notice Установить цену `newBuyPrice` eth для покупки и цену `newSellPrice` eth для продажи
    /// @param newSellPrice Цена, по которой пользователи могут продавать
    /// @param newBuyPrice Цена, по которой пользователи могут покупать
    function setPrices(uint256 newSellPrice, uint256 newBuyPrice) onlyOwner public {
        sellPrice = newSellPrice;
        buyPrice = newBuyPrice;
    }

    /// @notice Купить токены
    function buy() payable public returns (uint amount) {
        amount = msg.value / buyPrice;                    // Вычислияет сумму
        require(balanceOf[this] >= amount);               // Проверка, достаточный ли баланс у покупателя для продажи
        balanceOf[msg.sender] += amount;                  // Добавляет сумму к балансу покупателя
        balanceOf[this] -= amount;                        // Вычитает сумму из баланса продавца
        Transfer(this, msg.sender, amount);          
        return amount;                                    
    }

    /// @notice Продать `amount` токенов
    /// @param amount Количество токенов для продажи
    function sell(uint amount) public returns (uint revenue) {
        require(balanceOf[msg.sender] >= amount);         // Проверка, достаточный ли баланс у продавца для продажи
        balanceOf[this] += amount;                        // Добавляет сумму к балансу покупателя
        balanceOf[msg.sender] -= amount;                  // Вычитает сумму из баланса продавца
        revenue = amount * sellPrice;
        msg.sender.transfer(revenue);                     // Посылает эфир продавцу
        Transfer(msg.sender, this, amount);          
        return revenue;                                   
    }

    /* Нагража математеческому гению (вычислить кубический корень числа) */
    function rewardMathGeniuses(uint answerToCurrentReward, uint nextChallenge) public {
        require(answerToCurrentReward**3 == currentChallenge); // Проверка правильности ответа
        balanceOf[msg.sender] += 1;         // Награда игрока
        currentChallenge = nextChallenge;   // Установить следующее число
    }
}
