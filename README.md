# Контракты

## Список контрактов

* token.sol - контракт для создания собственной криптовалюты
* lottery.sol - контракт для создания криптовалюты для лотерии и розыгрыша лотереи

## token.sol

Данный контракт предназначен для создания собственной криптовалюты.

При создании данного контракта необходимо указать имя криптовалюты, его символ и количество валют для выпуска.

Контракт позволяет переводить токены другим пользователям (```transfer()```). А также сжигать (уничтожать) определенное количество токенов (```burn()```).

С помощью функции ```mintToken()``` можно создавать дополнительное количесвто токенов.

Также есть возможность блокировки или разблокировки пользователя с помощью функции ```freezeAccount()```.

Пользователи могут покупать (```buy()```) или продавать (```sell()```) токены по ценам указанным с помощью функции ```setPrices()```.

Созданы еще функция для получения пользователями токенов: ```rewardMathGeniuses()``` - пользователь получает награду за вычисление кубического корня числа, заданного предыдущим пользователям

## lottery.sol

Данный контракт предназначен для создания криптовалюты для лотерии и розыгрыша лотереи.

Здесь содержатся все функции контракта token.sol для создания криптовалюты для лотереи.

Купить билеты можно на созданные токены.

Пользователь покупает билеты под выбранным номером, если данный билет свободен (```buyTicket()```).

С помощью функии ```play()``` генерируется номер выиграшного билета. Пользователь с выиграшным билет получает джекпот. Также призы получают пользователи, номера билетов которых попадут в диапазон от предыдущего десятка и до следующего десятка. Например, если выиграл билет под номером 34, то призы получают пользователи, номера билетов которых входят в диапазон от 30 до 39 (<40).

Есть возможность изменить следующие параметры:
* цена билета;
* сумма, уходящая в джекпот;
* сумма приза